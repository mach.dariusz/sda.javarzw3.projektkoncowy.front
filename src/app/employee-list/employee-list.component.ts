import { Component, OnInit } from '@angular/core';
import { RestService } from '../service/rest.service';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {

  employees: Employee[] = [];

  constructor(private restService: RestService) { }

  ngOnInit() {
    console.log('start - execute ngOnInit()');

    this.employees.push({ firstName: 'Mariusz', lastName: 'Dach', salary: 2.34 });
    // this.employees.push({ firstName: 'Jan', lastName: 'Kowalski', salary: 12.22 });
    // this.employees.push({ firstName: 'John', lastName: 'Smith', salary: 42.19 });

    console.log('this.restService.getAllEmployees().subscribe()');
    this.restService.getAllEmployees().subscribe(data => {
      console.log('received results', data);
      setTimeout(() => {
        console.log('handle results', data);
        this.employees = data;
      }, 4000);
    });

    console.log('finish - execute ngOnInit()');
  }

}

export interface Employee {
  id?: string;
  firstName: string;
  lastName: string;
  salary: number;
}
